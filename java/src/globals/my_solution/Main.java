package globals.my_solution;

public class Main
{

    public static void main(String[] args)
    {

        var blue_boxes = new Blue_Boxes(15); // immediate feedback to what is used and with what value
        blue_boxes.fill_blue_apples(20);
        blue_boxes.some_useful_method_no_really();
        System.out.println(blue_boxes.box_count.intValue()); // all fine
        blue_boxes.evil_method_whose_only_purpose_is_to_annoy_you();
        System.out.println(blue_boxes.box_count.intValue()); // why the hell does this now cause a NPE?

        // But wait! We know that the error must be from a method that uses the "box_count" variable and we know that
        // this variable is only used in our Blue_Boxes class, no need to go hiking for a big quest to find all
        // the usages of some variable that uses this ridiculous primitive-type-boxing that is useless and annoying and I
        // should probably stop ranting...


        // But what if you say "But I know how many blue boxes and blue apples I want to start with and I do not want
        // to risk to type the wrong value in some different place."
        // Well, simply define methods/constructors that set these default values OR... you know... use default values.

    }

}
