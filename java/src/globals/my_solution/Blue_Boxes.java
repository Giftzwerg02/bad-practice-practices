package globals.my_solution;

public class Blue_Boxes
{
    public Integer box_count = 0;
    public Integer apple_count = 0;

    public Blue_Boxes(int box_count)
    {
        this.box_count = box_count;
    }

    public void fill_blue_apples(int apples)
    {
        apple_count += apples;
    }

    public void evil_method_whose_only_purpose_is_to_annoy_you()
    {
        box_count = null;
    }

    public void some_useful_method_no_really()
    {
        box_count = 5;
        apple_count++;
    }

}
