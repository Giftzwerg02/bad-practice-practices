package globals.problem;

public class Main
{

    /*
    * What are the problems with global-variable-spam?
    *   1. Why do we need them? Where is it used? Why even bother?
    *       It is possible to look up where a variable is used, IDEs might also help us with that.
    *       But why even get into the situation where you need to actively search for every single usage of
    *       every single variable. The "more" global a variable is, the more possible places pop into existence
    *       and therefore simply more file to look through to really find every point where a single variable is used in.
    *
    *   2. Which call truly caused the misbehaviour?
    *       Looking at the *very simple* example below, we use two method calls, one of which (maybe you can figure out
    *       which one I mean) contains a bug. Yet, since we used global variables all over the place, it might be quite
    *       difficult to figure out which method actually is at fault here.
    *       We got the NPE at the fourth instruction of the main method, but who says that therefore the mistake is at the
    *       third instruction?
    *
    *   3. Unnecessary bloat
    *       Again, IDEs are the MVPs here, but imagine we don't have one at hand right now. Would you be able to quickly
    *       say with confidence that we do not need some of the global variables that we defined? In this small example,
    *       sure, but what about a multi-million LOC project?
    *       If those variables where as close to their usage as possible, this would be much easier since their scope
    *       would be extraordinarily (english++) smaller.
    *
    */


    public static void main(String[] args)
    {

        some_useful_method_no_really();
        System.out.println(Global_Variables.amount_of_blue_apples_in_blue_box.intValue()); // all fine
        evil_method_whose_only_purpose_is_to_annoy_you();
        System.out.println(Global_Variables.amount_of_blue_apples_in_blue_box.intValue()); // why the hell does this now cause a NPE?

    }

    private static void evil_method_whose_only_purpose_is_to_annoy_you()
    {
        Global_Variables.amount_of_blue_apples_in_blue_box = null;
    }

    private static void some_useful_method_no_really()
    {
        Global_Variables.amount_of_blue_boxes = 5;
        Global_Variables.amount_of_blue_apples_in_blue_box++;
    }


}
