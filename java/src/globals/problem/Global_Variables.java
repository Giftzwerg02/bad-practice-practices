package globals.problem;

public class Global_Variables {

    public static Integer amount_of_red_apples_in_red_box = 23;
    public static Integer amount_of_green_apples_in_red_box = 20;
    public static Integer amount_of_blue_apples_in_red_box = 25;

    public static Integer amount_of_red_apples_in_green_box = 26;
    public static Integer amount_of_green_apples_in_green_box = 21;
    public static Integer amount_of_blue_apples_in_green_box = 12;

    public static Integer amount_of_red_apples_in_blue_box = 63;
    public static Integer amount_of_green_apples_in_blue_box = 2;
    public static Integer amount_of_blue_apples_in_blue_box = 20;

    public static Integer amount_of_red_boxes = 3;
    public static Integer amount_of_green_boxes = 2;
    public static Integer amount_of_blue_boxes = 15;

}
